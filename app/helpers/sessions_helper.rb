module SessionsHelper
  def log_in(user)
    session[:user_id] = user.id
  end

  def log_out
    session.delete(:user_id)
    @current_user = nil
  end

  def logged_in?
    !current_user.nil?
  end

  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def current_user_applicant?
    if current_user and current_user.privilege == 'applicant'
      return true
    end
    return false
  end

  def current_user_employer?
    if current_user and current_user.privilege == 'employer'
      return true
    end
    return false
  end
end
