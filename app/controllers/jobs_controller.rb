class JobsController < ApplicationController
  def show
    if current_user_applicant?
      user_id = current_user.id
      job_applied = Application.where(user_id: user_id).map { |a| a.job_id }
      @jobs = Job.where.not(id: job_applied)
      @companies = get_companies(@jobs)
    elsif current_user_employer?
      company_id = current_user.company_id
      @jobs = Job.where(company_id: company_id)
      @companies = get_companies(@jobs)
    else
      @jobs = Job.all
      @companies = get_companies(@jobs)
    end
  end

  def new
    if not logged_in?
      redirect_to root_path
    else
      @job = Job.new
    end
  end

  def create
    if current_user_employer?
      job_params = get_create_params
      job_params[:company_id] = current_user.company_id
      job = Job.new(job_params)
      if job.save
        @jobs = Job.all
        @companies = get_companies(@jobs)
        render "show"
      else
        render "new"
      end
    end
  end

  def details
    id = get_detail_params[:id]
    @job = Job.find(id)
    if @job
      user_ids = Application.where(job_id: @job.id).map {|a| a.user_id}
      @users = User.find(user_ids.to_a)
    end
  end

  def edit
    if current_user_applicant?
      id = get_detail_params[:id]
      application = Application.new(user_id: current_user.id, job_id: id)
      application.save
    end
    redirect_to jobs_path
  end

  private

  def get_create_params
    params.require(:job).permit(:title, :description)
  end

  def get_detail_params
    params.permit(:id)
  end

  def get_companies(jobs)
    companies_ids = jobs.map{ |j| j.company_id }.to_set.to_a
    Company.find(companies_ids).map{ |c| [c.id, c] }.to_h
  end
end
