class CompaniesController < ApplicationController
  def show
    @new_company = Company.new
    @companies = Company.all
  end

  def create
     params = company_params
     company = Company.new(params)
     company.save
     redirect_to company_path
  end

  def destroy
    company = Company.find_by(id: params[:id])
    if company
      company.destroy
    end
    redirect_to company_path
  end

  private

  def company_params
    params.require(:company).permit(:name, :domain)
  end
end
