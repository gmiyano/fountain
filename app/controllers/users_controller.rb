class UsersController < ApplicationController

  def new
    @user = User.new
  end

  def create
    user_params = get_params
    email = user_params[:email].strip
    domain = email.split("@").last
    company = Company.find_by(domain: domain)
    if company.nil?
      user_params[:privilege] = "applicant"
      @user = User.new(user_params)
    else
      user_params[:privilege] = "employer"
      user_params[:company_id] = company.id
      @user = User.new(user_params)
    end
    if @user.save
      log_in(@user)
      redirect_to jobs_path
    else
      render "new"
    end
  end

  private

  def get_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
