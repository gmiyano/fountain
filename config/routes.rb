Rails.application.routes.draw do
  root   'welcome#home'
  get    '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get    '/job/:id', to: 'jobs#details', as: :job
  get    '/apply/:id', to: 'jobs#edit', as: :apply
  get    '/company', to: 'companies#show'
  post   '/company', to: 'companies#create'
  delete '/company/:id', to: 'companies#destroy', as: :delete_company


  resource :users, only: [:create]
  resource :jobs
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
